<?php

namespace Modules\Portal\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Modules\User\Models\User;
use Mollie\Api\MollieApiClient;

class Order extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'user_id',
        'name',
        'customer_id',
        'order_id',
        'payment_id',
        'subscription_id',
        'mandate_id',
        'payment_status',
    ];

    const STATUS_OPEN = "open";
    const STATUS_PENDING = "pending";
    const STATUS_CANCELED = "canceled";
    const STATUS_EXPIRED = "expired";
    const STATUS_PAID = "paid";
    const STATUS_FAILED = "failed";

    // relation to other database
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function createOrder($mollie_customer_id)
    {
        $order = Order::create([
            'user_id' => Auth::user()->id,
            'name' => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            'customer_id' => $mollie_customer_id,
            'order_id' => '',
            'payment_id' => '',
            'subscription_id' => '',
            'mandate_id' => '',
            'payment_status' => Order::STATUS_OPEN,
        ]);

        return $order;
    }

    public function updatePaymentOrder($paymentId)
    {
        Order::where('user_id',Auth::user()->id)->orderBy('id','desc')->first()->update(['payment_id'=>$paymentId]);
    }

    public function updateOrderId($orderId)
    {
        Order::where('user_id',Auth::user()->id)->orderBy('id','desc')->first()->update(['order_id'=>$orderId]);
    }

    // get mandate Mollie
    public function getMandate()
    {
        $mollie = new MollieApiClient();
        $mollie->setApiKey(config("mollie.api_key"));
        $mollie_customer_id = \App\User::where('id',Auth::user()->id)->pluck('customer_id')->first();
        $customer = $mollie->customers->get($mollie_customer_id);
        $mandates = $customer->mandates();
        foreach ($mandates as $mandate) {
            $mandateId = $mandate->id;
            $mandate1 = $customer->getMandate($mandateId);
//            Order::where('user_id',Auth::user()->id)->update(['mandate_id' => $mandateId]);
            Order::where('user_id',Auth::user()->id)->orderBy('id','desc')->first()->update(['mandate_id'=>$mandateId]);
            return  $mandateId;
        }
    }

    public function saveSubscriptionId()
    {
        $mollie = new MollieApiClient();
        $mollie->setApiKey(config("mollie.api_key"));
        $mollie_customer_id = \App\User::where('id',Auth::user()->id)->pluck('customer_id')->first();
        $customer = $mollie->customers->get($mollie_customer_id);
        $subscriptions = $customer->subscriptions();
        foreach ($subscriptions as $sub){
            $subscriptionId = $sub->id;
//            Order::where('user_id',Auth::user()->id)->update(['subscription_id' => $subscriptionId]);
            Order::where('user_id',Auth::user()->id)->orderBy('id','desc')->first()->update(['subscription_id'=>$subscriptionId]);
        }
    }


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [

    ];
}
