<?php

namespace Modules\Portal\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Modules\Portal\Models\Email;
use Modules\Portal\Models\Payment;
use Modules\Portal\Models\Order;
use App\User;
use Illuminate\Support\Facades\Auth;
use Mollie\Api\Exceptions\ApiException;
use Mollie\Api\MollieApiClient;
//use Mollie\Api\Resources\Order;
use Mollie\Api\Resources\Subscription;
use Mollie\Api\Resources\SubscriptionCollection;
use Mollie\Api\Types\OrderStatus;

class MolliePortalController extends Controller
{
    // Single payment
    public function createSinglePayment()
    {

        $price = Payment::all()->last();

        $mollie = new \Mollie\Api\MollieApiClient();
        $mollie->setApiKey(config("mollie.api_key"));

        $payment = $mollie->payments->create([
            "amount" => [
                "currency" => "EUR",
                'value' => $price->amount,
            ],
            "description" => "My first API payment",
            "redirectUrl" => route('mollie.payment.success'),
            "webhookUrl" => config('mollie.dev_mode') ? config('mollie.dev_webhook') : route('mollie.webhook'),
            "metadata" => [
                "order_id" => "12345",
            ],
        ]);


//        return response(['succes' => true]);
        return redirect($payment->getCheckoutUrl());
    }

    // create payment for linking user to mollie for a subscription
    public function createInitialPayment()
    {
        $order = new Order();

        $mollie = new MollieApiClient();
        $mollie->setApiKey(config("mollie.api_key"));

        $orderId = time();

//        dd($orderId);

        // check if customer already created or not
        $mollie_customer_id = User::where('id',Auth::user()->id)->pluck('customer_id')->first();

        $name = Auth::user()->first_name . ' ' . Auth::user()->last_name;
        $email = Auth::user()->email;
        if (empty($mollie_customer_id)) {

            /**
             * @throw MollieCreateCustomerException
             */
            try {
                $customer = $mollie->customers->create([
                    "name" => $name,
                    "email" => $email,
                ]);
            } catch (ApiException $e) {
                echo $e->getMessage();
            }

            $mollie_customer_id = $customer->id;

            User::where('id',Auth::user()->id)->update(['customer_id'=>$mollie_customer_id]);
        }

        $order->createOrder($mollie_customer_id);
        $order->updateOrderId($orderId);

        $price = Payment::all()->last();

        // Creating Payment
        $payment = $mollie->customers->get($mollie_customer_id)->createPayment([
            "amount" => [
                "currency" => "EUR",
                "value" => $price->amount,
            ],
            "description" => "description",
            "sequenceType" => "first",
            "webhookUrl" => config('mollie.dev_mode') ? config('mollie.dev_webhook') : route('mollie.webhook'),
            "redirectUrl" => route('mollie.payment.success'),
            "metadata" => [
                "order_id" => $orderId,
            ],
        ]);

        $paymentId = $payment->id;

        $order->updatePaymentOrder($paymentId);

        return redirect($payment->getCheckoutUrl(), 303);
    }

    /**
     * Create Mollie Subscription
     *
     * @return Response
     */
    public function createMollieSubscription()
    {
        $mollie = new MollieApiClient();
        $mollie->setApiKey(config("mollie.api_key"));
        $random = rand(1,30);

        $order = New Order();
        $mandateId = $order->getMandate();

        $price = Payment::all()->last();
        $interval = $price->interval;

        $mollie_customer_id = User::where('id',Auth::user()->id)->pluck('customer_id')->first();
        $customer = $mollie->customers->get($mollie_customer_id);

            $subscription = $customer->createSubscription([
                "amount" => [
                    "currency" => "EUR",
                    "value" => $price->amount,
                ],
                "description" => "Subscription {$random}",
                "interval" => "1" . ' '. $interval,
                "webhookUrl" => config('mollie.dev_mode') ? config('mollie.dev_webhook') : route('mollie.webhook'),
                "mandateId" => $mandateId,
            ]);

        $order->saveSubscriptionId();

        return redirect('/molliepayment');
    }


    public function cancelSubscription()
    {
        $mollie = new MollieApiClient();
        $mollie->setApiKey(config("mollie.api_key"));

        $mollie_customer_id = User::where('id',Auth::user()->id)->pluck('customer_id')->first();

        $customer = $mollie->customers->get($mollie_customer_id);

        $subscriptions = $customer->subscriptions();
        foreach ($subscriptions as $sub){
            $subscriptionId = $sub->id;
            $customer->cancelSubscription($subscriptionId);
        }
        return redirect('/molliepayment');
    }


    public function updateSubscription()
    {
        $mollie = new MollieApiClient();
        $mollie->setApiKey(config("mollie.api_key"));

        $mollie_customer_id = User::where('id',Auth::user()->id)->pluck('customer_id')->first();

        $customer = $mollie->customers->get($mollie_customer_id);

        $subscriptions = $customer->subscriptions();
        foreach ($subscriptions as $sub){
            $subscriptionId = $sub->id;
            $subscription = $customer->getSubscription($subscriptionId);

            $subscription->amount = (object)
            ['value' => '12.12',
            'currency' => 'EUR'];
            $subscription->description = 'Monthly subscription';
            $subscription->interval = '1 days';
            $subscription->startDate = '2022-12-25'; // Year-month-day
            $subscription->times = '12';
            $subscription->update();
        }
        return redirect('/molliepayment');
    }
}
